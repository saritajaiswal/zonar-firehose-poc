package com.vcl.pubsubdemo.controller;

import com.vcl.pubsubdemo.model.MessageDetails;
import com.vcl.pubsubdemo.service.FirehoseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping(path = "/api")
public class FirehoseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FirehoseController.class);

    @Autowired
    private FirehoseService firehoseService;

    @PostMapping(path="/firehose")
    public ResponseEntity<?> createPublisher(@RequestBody MessageDetails messageObj, @RequestParam(required = false)String bigquery_dataset,@RequestParam(required = false)String dataset_table) throws IOException, InterruptedException {

        firehoseService.firehoseService(messageObj,bigquery_dataset,dataset_table);
        return new ResponseEntity<>("Successfully wrote on firehose API",HttpStatus.OK);
    }
}
