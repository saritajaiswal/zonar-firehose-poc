package com.vcl.pubsubdemo.controller;

import com.vcl.pubsubdemo.model.MessageDetails;
import com.vcl.pubsubdemo.service.PubSubService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping(path = "/pubsub")
public class PubSubController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PubSubController.class);

    @Autowired
    private PubSubService pubSubService;

     @PostMapping("/topic")
     public ResponseEntity<?>createPublisher(@RequestParam String projectId, @RequestParam String topic, @RequestBody MessageDetails message,@RequestParam(required = false)String bigquery_dataset,@RequestParam(required = false)String dataset_table) throws IOException, InterruptedException {
        String result=pubSubService.createPublisher(projectId,topic,message,bigquery_dataset,dataset_table);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @PostMapping("/subscriber")
    public ResponseEntity<?>createSubscriber(@RequestParam String projectId,@RequestParam String subscriptionId) throws IOException, InterruptedException {
        String result=pubSubService.createSubscriber(projectId,subscriptionId);
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

}
