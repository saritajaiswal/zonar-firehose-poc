package com.vcl.pubsubdemo.model;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageDetails {

	private String vcllinkserialnumber;
	private String lastlicenseplate;
	private String vin;
}
