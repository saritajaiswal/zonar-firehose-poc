package com.vcl.pubsubdemo.service;

import com.google.cloud.bigquery.*;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class BigqueryServiceImpl implements BigqueryService{

    @Override
    public String fetchBigQuery(String projectID,String bigquery_dataset,String dataset_table) throws IOException, InterruptedException  {

        BigQuery bigquery = BigQueryOptions.getDefaultInstance().getService();
        Dataset dataset = bigquery.getDataset(bigquery_dataset);

        QueryJobConfiguration queryConfig =
                QueryJobConfiguration.newBuilder(
                        "SELECT * "
                                //+ "FROM `vcl-working-a5d6.vcl_bigquery.message_desc` "
                                + "FROM" + "`"+projectID+"."+bigquery_dataset+"."+dataset_table+"`"
                                + "LIMIT 10")
                        .setUseLegacySql(false)
                        .build();

        // Create a job ID so that we can safely retry.
        JobId jobId = JobId.of(UUID.randomUUID().toString());
        Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());

        // Wait for the query to complete.
        queryJob = queryJob.waitFor();

        // Check for errors
        if (queryJob == null) {
            throw new RuntimeException("Job no longer exists");
        } else if (queryJob.getStatus().getError() != null) {
            throw new RuntimeException(queryJob.getStatus().getError().toString());
        }
        TableResult result = queryJob.getQueryResults();
        // Print all pages of the results.
        for (FieldValueList row : result.iterateAll()) {
            // String type
            String vin = row.get("vin").getStringValue();
            String ser_no = row.get("serial_number").getStringValue();
            String plate = row.get("plate").getStringValue();
            System.out.printf(
                    "Vin name: %s, %s, %s \n", vin, ser_no, plate);
        }
        return "Successfully fetched BigQuery";
    }

    @Override
    public String publishBigQuery(String projectID,String bigquery_dataset,String dataset_table) throws IOException, InterruptedException  {

        BigQuery bigquery = BigQueryOptions.getDefaultInstance().getService();
        Dataset dataset = bigquery.getDataset(bigquery_dataset);

        TableId tableId = TableId.of(bigquery_dataset, dataset_table);
        // Values of the row to insert
        Map rowContent = new HashMap();
        rowContent.put("serial_number", 105);
        rowContent.put("vin", 9002);
        rowContent.put("plate", "AB03");
        InsertAllResponse response = bigquery.insertAll(InsertAllRequest.newBuilder(tableId).addRow("rowId", rowContent)
                // More rows can be added in the same RPC by invoking .addRow() on the
                // builder
                .build());
        if (response.hasErrors()) {
            // If any of the insertions failed, this lets you inspect the errors
            for (Map.Entry entry : response.getInsertErrors().entrySet()) {
                // inspect row error
            }
        }

        return "Successfully loaded BigQuery";
    }
}
