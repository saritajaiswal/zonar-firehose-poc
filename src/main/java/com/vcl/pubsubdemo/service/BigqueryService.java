package com.vcl.pubsubdemo.service;

import java.io.IOException;

public interface BigqueryService {
    String fetchBigQuery(String projectID,String bigquery_dataset,String dataset_table) throws IOException, InterruptedException;
    String publishBigQuery(String projectID,String bigquery_dataset,String dataset_table) throws IOException, InterruptedException;
}
