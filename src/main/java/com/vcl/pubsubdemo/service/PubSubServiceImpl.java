package com.vcl.pubsubdemo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.core.ApiFuture;
import com.google.cloud.pubsub.v1.AckReplyConsumer;
import com.google.cloud.pubsub.v1.MessageReceiver;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.cloud.pubsub.v1.Subscriber;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectSubscriptionName;
import com.google.pubsub.v1.PubsubMessage;
import com.google.pubsub.v1.TopicName;

import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobId;
import com.google.cloud.bigquery.JobInfo;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableResult;
import com.google.cloud.bigquery.Dataset;
import com.google.cloud.bigquery.TableId;
import com.google.cloud.bigquery.InsertAllResponse;
import com.google.cloud.bigquery.InsertAllRequest;

import com.vcl.pubsubdemo.model.MessageDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
public class PubSubServiceImpl implements PubSubService{

    private static final Logger LOGGER = LoggerFactory.getLogger(PubSubServiceImpl.class);

    @Autowired
    private BigqueryService bigqueryService;

    @Override
    public String createPublisher(String projectId, String topicId, MessageDetails messageObj,String bigquery_dataset,String dataset_table) throws IOException, InterruptedException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
        String  timestamp = new Timestamp(System.currentTimeMillis()).toString();
        LOGGER.info(timestamp);

        ObjectMapper Obj = new ObjectMapper();
        String message = Obj.writeValueAsString(messageObj);

        Publisher publisher = null;

        try {

            TopicName topicName = TopicName.of(projectId, topicId);
            // Create a publisher instance with default settings bound to the topic
            publisher = Publisher.newBuilder(topicName).build();
            ByteString data = ByteString.copyFromUtf8(message);
            PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();

            // Once published, returns a server-assigned message id (unique within the topic)
            ApiFuture<String> messageIdFuture = publisher.publish(pubsubMessage);
            String messageId = messageIdFuture.get();
            LOGGER.info("Published message ID: "+ messageId);

           if(bigquery_dataset!=null && dataset_table!=null) {
                bigqueryService.publishBigQuery(projectId, bigquery_dataset, dataset_table);
                bigqueryService.fetchBigQuery(projectId, bigquery_dataset, dataset_table);
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
            // When finished with the publisher, shutdown to free up resources.
            if (publisher != null) {
                publisher.shutdown();
                publisher.awaitTermination(1, TimeUnit.MINUTES);
            }

        }
        return "created publisher";
    }

    @Override
    public String createSubscriber(String projectId, String subscriptionId) {
        ProjectSubscriptionName subscriptionName =
                ProjectSubscriptionName.of(projectId, subscriptionId);
        // Instantiate an asynchronous message receiver.
        MessageReceiver receiver =
                (PubsubMessage message, AckReplyConsumer consumer) -> {
                    // Handle incoming message, then ack the received message.
                    LOGGER.info("Id: "+ message.getMessageId());
                    LOGGER.info("Data: "+ message.getData().toStringUtf8());
                    consumer.ack();
                };

        Subscriber subscriber = null;
        try {
            subscriber = Subscriber.newBuilder(subscriptionName, receiver).build();
            // Start the subscriber.
            subscriber.startAsync().awaitRunning();
            LOGGER.info("Listening for messages on %s:\n"+ subscriptionName.toString());
            // Allow the subscriber to run for 30s unless an unrecoverable error occurs.
            subscriber.awaitTerminated(30, TimeUnit.SECONDS);
        } catch (TimeoutException timeoutException) {
            // Shut down the subscriber after 30s. Stop receiving messages.
            subscriber.stopAsync();
        }
        return "created subscriber";
    }

    public String fetchBigQuery() throws IOException, InterruptedException  {

        BigQuery bigquery = BigQueryOptions.getDefaultInstance().getService();
        Dataset dataset = bigquery.getDataset("vcl_bigquery");

        QueryJobConfiguration queryConfig =
                QueryJobConfiguration.newBuilder(
                        "SELECT * "
                                + "FROM `vcl-working-a5d6.vcl_bigquery.message_desc` "
                                + "LIMIT 10")
                        .setUseLegacySql(false)
                        .build();

        // Create a job ID so that we can safely retry.
        JobId jobId = JobId.of(UUID.randomUUID().toString());
        Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());

        // Wait for the query to complete.
        queryJob = queryJob.waitFor();

        // Check for errors
        if (queryJob == null) {
            throw new RuntimeException("Job no longer exists");
        } else if (queryJob.getStatus().getError() != null) {
            throw new RuntimeException(queryJob.getStatus().getError().toString());
        }
        TableResult result = queryJob.getQueryResults();
        // Print all pages of the results.
        for (FieldValueList row : result.iterateAll()) {
            // String type
            String vin = row.get("vin").getStringValue();
            String ser_no = row.get("serial_number").getStringValue();
            String plate = row.get("plate").getStringValue();
            System.out.printf(
                    "Vin name: %s, %s, %s \n", vin, ser_no, plate);
        }
        return "Successfully fetched BigQuery";
    }

    public String publishBigQuery() throws IOException, InterruptedException  {

        BigQuery bigquery = BigQueryOptions.getDefaultInstance().getService();
        Dataset dataset = bigquery.getDataset("vcl_bigquery");

        TableId tableId = TableId.of("vcl_bigquery", "message_desc");
        // Values of the row to insert
        Map rowContent = new HashMap();
        rowContent.put("serial_number", 105);
        rowContent.put("vin", 9002);
        rowContent.put("plate", "AB03");
        InsertAllResponse response = bigquery.insertAll(InsertAllRequest.newBuilder(tableId).addRow("rowId", rowContent)
                // More rows can be added in the same RPC by invoking .addRow() on the
                // builder
                .build());
        if (response.hasErrors()) {
            // If any of the insertions failed, this lets you inspect the errors
            for (Map.Entry entry : response.getInsertErrors().entrySet()) {
                // inspect row error
            }
        }

        return "Successfully loaded BigQuery";
    }
}