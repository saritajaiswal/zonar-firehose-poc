package com.vcl.pubsubdemo.service;

import com.vcl.pubsubdemo.model.MessageDetails;

import java.io.IOException;

public interface PubSubService {

    String createPublisher(String projectId, String topicId, MessageDetails messageObj,String bigquery_dataset,String dataset_table) throws IOException, InterruptedException;

    String createSubscriber(String projectId, String subscriptionId);
}
