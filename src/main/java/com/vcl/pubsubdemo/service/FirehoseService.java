package com.vcl.pubsubdemo.service;

import com.vcl.pubsubdemo.model.MessageDetails;

import java.io.IOException;

public interface FirehoseService {
    void firehoseService(MessageDetails messageObj,String bigquery_dataset,String bigquery_table) throws IOException, InterruptedException;
}
